{{-- layout --}}
@extends('layouts.contentLayoutMaster')

{{-- page title --}}
@section('title', 'Attribute')

{{-- vendor styles --}}
@section('vendor-style')
<link rel="stylesheet" type="text/css" href="{{ asset('vendors/flag-icon/css/flag-icon.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('vendors/dropify/css/dropify.min.css') }}">
@endsection

@section('content')
<div class="section">
    <div class="row">
        <div class="col s12">
            <div id="html-validations" class="card card-tabs">
                <div class="card-content">
                    <div class="card-title">
                        <div class="row">
                            <div class="col s12 m6 l10">
                                <h4 class="card-title">Update</h4>
                            </div>
                            <div class="col s12 m6 l2">
                            </div>
                        </div>
                    </div>
                    <div id="html-view-validations">
                        <form id="addform" class="forms-sample" method="POST"
                            action="{{ route('admin.att.update', $attribute) }}" enctype="multipart/form-data">
                            @csrf
                            @method('PUT')
                            <div class="row">
                                <div class="input-field col s12 m6 l6">
                                    <input type="text" name="title" id="title" class="validate"
                                        value="{{ $attribute->title }}" required />
                                    <label for="title">Title*</label>
                                </div>

                                {{-- Options --}}
                                <div class="col s12">
                                    <label for="options">Options</label>
                                    @if ($attribute->options)
                                    @foreach ($attribute->options as $key => $item)
                                    <div class="row mb-2">
                                        <input type="hidden" name="option_old[{{ $key }}][id]" value="{{ $item->id }}">
                                        <div class="input-field col s12 m3 l3">
                                            <input type="text" name="option_old[{{ $key }}][name]"
                                                class="form-control" value="{{ $item->name }}"
                                                placeholder="Name">
                                        </div>
                                        <div class="input-field col s12 m3 l3">
                                            <input type="number" step="0.1" name="option_old[{{ $key }}][price]"
                                                class="form-control" value="{{ $item->price }}"
                                                placeholder="price">
                                        </div>

                                        <button type="button" class="btn-floating red pulse ml-2 btn-remove-old-pn">
                                            <i class="material-icons">delete</i>
                                        </button>
                                    </div>
                                    @php

                                    @endphp
                                    @endforeach
                                    @endif
                                    <div class="repeater">
                                        <div data-repeater-list="options">
                                            <div data-repeater-item class="row mb-2">
                                                <div class="input-field col s12 m3 l3">
                                                    <input type="text" name="name" class="form-control"
                                                        placeholder="Name">
                                                </div>
                                                <div class="input-field col s12 m3 l3">
                                                    <input type="number" step="0.1" name="price" class="form-control"
                                                        placeholder="Price">
                                                </div>
                                                <button data-repeater-delete type="button"
                                                    class="btn-floating red pulse">
                                                    <i class="material-icons">delete</i>
                                                </button>
                                            </div>
                                        </div>
                                        <button data-repeater-create type="button"
                                            class="btn-floating cyan pulse right">
                                            <i class="material-icons">add</i>
                                        </button>
                                    </div>
                                </div>
                                {{-- end options --}}


                                <div class="input-field col s12">
                                    <button class="btn cyan waves-effect waves-light right" type="submit"
                                        name="action">{{ __('admin-content.submit') }}
                                        <i class="material-icons right">send</i>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog"
                        aria-labelledby="exampleModalCenterLabel" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalCenterLabel">
                                        {{ __('admin-content.delete-image') }}
                                    </h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                            aria-hidden="true">&times;</span></button>
                                </div>
                                <div class="modal-body">
                                    {{ __('admin-content.are-you-sure-you-need-to-delete-this') }}
                                    {{ __('admin-content.image') }}?
                                </div>
                                <div class="modal-footer">
                                    <form id="frm_confirm_delete_multiple_image" action="#" method="POST">
                                        @csrf
                                        <input type="hidden" value="" name="id" id="item_id">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{
                                            __('admin-content.close') }}</button>
                                        <button type="submit" class="btn btn-primary" href="">{{
                                            __('admin-content.delete') }}</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
{{-- vendor script --}}
@section('vendor-script')
<script src="{{ asset('vendors/dropify/js/dropify.min.js') }}"></script>
<script src="{{ asset('vendors/jquery-validation/jquery.validate.min.js') }}"></script>

@endsection

{{-- page script --}}
@section('page-script')
<script src="{{ asset('js/scripts/form-file-uploads.js') }}"></script>
<script src="{{ asset('js/scripts/form-validation.js') }}"></script>
<script src="{{ asset('plugins/jquery.repeater/jquery.repeater.min.js') }}"></script>
<script>
    $('.btn-remove-old-pn').click(function (e) {
        if (confirm('Are you sure you want to delete this element?')) {
            $(this).parent().remove();
        }
    });
    $('.repeater').repeater({
        initEmpty: true,
        show: function () {
            $(this).slideDown();
            $(function () {
            });
        },
        hide: function (deleteElement) {
            if (confirm('Are you sure you want to delete this element?')) {
                $(this).slideUp(deleteElement);
            }
        },
        isFirstItemUndeletable: true
    });
</script>

@endsection

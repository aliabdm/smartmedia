{{-- layout --}}
@extends('layouts.contentLayoutMaster')

{{-- page title --}}
@section('title', 'Attribute')

{{-- vendor styles --}}
@section('vendor-style')
<link rel="stylesheet" type="text/css" href="{{ asset('vendors/flag-icon/css/flag-icon.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('vendors/dropify/css/dropify.min.css') }}">
@endsection

@section('content')
<div class="section">
    <div class="row">
        <div class="col s12">
            <div id="html-validations" class="card card-tabs">
                <div class="card-content">
                    <div class="card-title">
                        <div class="row">
                            <div class="col s12 m6 l10">
                                <h4 class="card-title">Add New</h4>
                            </div>
                            <div class="col s12 m6 l2">
                            </div>
                        </div>
                    </div>
                    <div id="html-view-validations">
                        <form class="formValidate0" id="formValidate0" method="POST"
                            action="{{ route('admin.att.store') }}" enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <input type="hidden" name='product_id' value="{{ $id }}">
                                <div class="input-field col s12 m6 l6">
                                    <input type="text" name="title" id="title" class="validate" required />
                                    <label for="title">Title*</label>
                                </div>
                                {{-- Option Values --}}
                                <div class="col s12">
                                    <label for="options">Option Values</label>
                                    <div class="repeater" id="option_repeater">
                                        <div data-repeater-list="options">
                                            <div data-repeater-item class="row mb-2">
                                                <div class="input-field col s12 m3 l3">
                                                    <input type="text" name="name" class="form-control"
                                                        placeholder="Name">
                                                </div>
                                                <div class="input-field col s12 m3 l3">
                                                    <input type="number" step="0.1" name="price" class="form-control"
                                                        placeholder="Price">
                                                </div>
                                                <button data-repeater-delete type="button"
                                                    class="btn-floating red pulse">
                                                    <i class="material-icons">delete</i>
                                                </button>
                                            </div>
                                        </div>
                                        <button data-repeater-create type="button"
                                            class="btn-floating cyan pulse right">
                                            <i class="material-icons">add</i>
                                        </button>
                                    </div>
                                </div>
                                {{-- end of contact us --}}

                                <div class="input-field col s12">
                                    <button class="btn cyan waves-effect waves-light right" type="submit"
                                        name="action">{{ __('admin-content.submit') }}
                                        <i class="material-icons right">send</i>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

{{-- vendor script --}}
@section('vendor-script')
<script src="{{ asset('vendors/dropify/js/dropify.min.js') }}"></script>
<script src="{{ asset('vendors/jquery-validation/jquery.validate.min.js') }}"></script>
@endsection

{{-- page script --}}
@section('page-script')
<script src="{{ asset('js/scripts/form-file-uploads.js') }}"></script>
<script src="{{ asset('js/scripts/form-validation.js') }}"></script>
<script src="{{ asset('plugins/jquery.repeater/jquery.repeater.min.js') }}"></script>

<script>
    $('#option_repeater').repeater({
    initEmpty: true,
    show: function() {
        $(this).slideDown();
    },
    hide: function(deleteElement) {
        if (confirm('Are you sure you want to delete this element?')) {
            $(this).slideUp(deleteElement);
        }
    },
    isFirstItemUndeletable: true
  });

  $('.repeater').repeater({
     initEmpty: true,
     show: function () {
         $(this).slideDown();
     },
     hide: function (deleteElement) {
         if (confirm('Are you sure you want to delete this element?')) {
             $(this).slideUp(deleteElement);
         }
     },
     isFirstItemUndeletable: true
 });
</script>

@endsection

<?php

use Illuminate\Support\Facades\Route;

Route::prefix('admin')->namespace('Admin')->name('admin.')->group(function () {
    Route::get('/login', 'Auth\AuthController@showLoginForm')->name('login_form');
    Route::post('/login', 'Auth\AuthController@login')->name('login');



    Route::middleware(['auth:admin'])->group(function () {
        Route::get('/', function () {
            return redirect()->route('admin.dashboard');
        });

        Route::get('/dashboard', 'HomeController@dashboard')->name('dashboard');
        Route::get('/logout', 'Auth\AuthController@logout')->name('logout');

        #Admin profile routes
        Route::get('/profile', 'UserController@profile')->name('profile');
        Route::post('/profile/update', 'UserController@updateProfile')->name('profile.update');
    });

    #Administration routes
    Route::middleware(['auth:admin', 'permission:admin management'])->group(function () {
        Route::resource('administration', 'UserController')->name('*', 'administration');
    });

    #Customers routes
    Route::middleware(['auth:admin', 'permission:customers management'])->group(function () {
        Route::resource('customer', 'CustomerController')->name('*', 'customer');
    });

    #Product routes
    Route::middleware(['auth:admin', 'permission:products management'])->group(function () {
        Route::resource('product', 'ProductController')->name('*', 'product');
        Route::get('product/attributes/{id}','ProductController@att')->name('product.att');
        Route::get('attribute-create/{id}','AttributeController@create')->name('att.create');
        Route::post('attribute-store','AttributeController@store')->name('att.store');
        Route::get('attribute-show/{id}','AttributeController@show')->name('att.show');
        Route::put('attribute/{id}','AttributeController@update')->name('att.update');
        Route::delete('attributes-delete/{id}','AttributeController@destroy')->name('att.destroy');
    });
});

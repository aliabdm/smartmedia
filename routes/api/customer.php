<?php

use Illuminate\Support\Facades\Route;

Route::prefix('customer')->namespace('API\Customer')->middleware('apilogger')->group(function () {

    # Auth Routes
    Route::post('login', 'AuthController@login');
    Route::post('register', 'AuthController@register');

    Route::middleware(['api', 'multiauth:customer'])->group(function () {
        # profile
        Route::get('profile', 'AuthController@profile');
        Route::post('profile', 'AuthController@updateProfile');

        Route::get('logout', 'AuthController@logout');
        # dashboard
        Route::get('dashboard', 'DashboardController@dashboard');

        Route::get('products','ProductController@list');
        Route::get('product/{product}','ProductController@show');
    });
});

<?php

namespace App\Models;

use App\Enums\OrderStatus;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use SMartins\PassportMultiauth\HasMultiAuthApiTokens;

class Customer extends Authenticatable
{
    use Notifiable, HasMultiAuthApiTokens;

    protected $fillable = ['username', 'password'];
    protected $hidden = ['password', 'remember_token'];
    protected $casts = ['account_verified_at' => 'datetime'];

    public function profile()
    {
        return $this->hasOne(CustomerProfile::class);
    }

    public function findForPassport($username)
    {
        return $this->where([
            ['username', '=', $username],
            ['account_verified_at', '!=', null],
        ])->first();
    }

}

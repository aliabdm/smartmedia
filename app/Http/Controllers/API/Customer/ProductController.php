<?php

namespace App\Http\Controllers\API\Customer;

use App\Http\Controllers\Controller;
use App\Http\Resources\ProductCollection;
use App\Http\Resources\ProductResource;
use App\Models\Product;
use Illuminate\Http\Request;
use App\Traits\ApiResponser;

class ProductController extends Controller
{
    use ApiResponser;

    public function list()
    {
        $products = Product::all();

        return $this->successResponse(200, trans('api.public.done'), 200, new ProductCollection($products));

    }

    public function show(Product $product)
    {
        return $this->successResponse(200, trans('api.public.done'), 200, new ProductResource($product));
    }

}

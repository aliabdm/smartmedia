<?php

namespace App\Http\Controllers\API\Customer;

use App\Http\Controllers\Controller;
use App\Http\Resources\CourseProgressCollection;
use App\Http\Resources\ReviewCollection;
use App\Traits\ApiResponser;

class DashboardController extends Controller
{
    use ApiResponser;

    public function dashboard()
    {
    }
}

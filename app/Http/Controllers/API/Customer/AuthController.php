<?php

namespace App\Http\Controllers\API\Customer;

use App\Enums\GenderTypes;
use App\Enums\SourceType;
use App\Models\Customer;
use App\Models\CustomerProfile;
use App\Http\Controllers\Controller;
use App\Http\Requests\Customer\ChangePasswordRequest;
use App\Http\Requests\Customer\ForgetPasswordRequest;
use App\Http\Requests\Customer\LinkSocialRequest;
use App\Http\Requests\Customer\LoginRequest;
use App\Http\Requests\Customer\ProfileRequest;
use App\Http\Requests\Customer\RegisterRequest;
use App\Http\Requests\Customer\ResendSMSRequest;
use App\Http\Requests\Customer\ResetPasswordRequest;
use App\Http\Requests\Customer\VerifySMSRequest;
use App\Http\Resources\Customer as ResourcesCustomer;
use App\Models\CustomerInterest;
use App\Models\CustomerProvider;
use App\Models\Dialog;
use App\Services\Verification;
use App\Traits\ApiResponser;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Str;
use Exception;
use Illuminate\Database\QueryException;
use Laravel\Passport\Client as OClient;
use Illuminate\Support\Facades\File;
use Intervention\Image\ImageManagerStatic as Image;

class AuthController extends Controller
{
    use ApiResponser;

    public function login(LoginRequest $request)
    {
        $http = new \GuzzleHttp\Client([
            'base_uri' => 'http://127.0.0.1:8000',
        ]);

        try {
            $customer = Customer::where('username', $request->username)->where('is_blocked', true)->first();

            if ($customer) {
                return $this->errorResponse(206, trans('api.auth.blocked'), 400);
            }

            $oClient = OClient::where('password_client', 1)->first();

            $response = $http->request('POST','/oauth/token', [
                'form_params' => [
                    'grant_type' => 'password',
                    'client_id' => $oClient->id,
                    'client_secret' => $oClient->secret,
                    'username' => $request->username,
                    'password' => $request->password,
                    'provider' => 'customers',
                    'scope' => '*',
                ],
            ]);

            $customer = Customer::where('username', $request->username)->first();


            $result = [
                'token' => json_decode($response->getBody())
            ];

            return $this->successResponse(200, trans('api.auth.login'), 200, $result);
        } catch (\GuzzleHttp\Exception\BadResponseException $e) {
            switch ($e->getCode()) {
                case 400:
                case 401:
                    return $this->errorResponse($e->getCode(), trans('api.auth.invalid_credentials'), $e->getCode());
                    break;
                default:
                    return $this->errorResponse($e->getCode(), trans('api.public.server_error'), $e->getCode());
                    break;
            }
        }
    }

    public function register(RegisterRequest $request)
    {
        $customer = new Customer($request->all());
        $customer->username = $request->username;
        $customer->password = bcrypt($request->password);
        $customer->account_verified_at = now();

        $customer->save();

        $customerProfile = new CustomerProfile();
        $customerProfile->customer_id = $customer->id;
        $customerProfile->full_name = trim($request->full_name);
        $customerProfile->save();

        try {
            // $twilio = new Verification($customer->username);
            // $twilio->sendSms();
        } catch (\Exception $e) {
            return $this->errorResponse(401, $e->getMessage(), 401);
        }

        return $this->successResponse(200, trans('api.auth.register'), 200, null);
    }

    public function logout()
    {
        auth()->user()->token()->delete();
        return $this->successResponse(200, trans('api.auth.loggedout'), 200, null);
    }

    public function logoutFromOtherDevices()
    {
        $current_token = auth()->user()->token()->id;
        auth()->user()->tokens()->each(function ($token) use ($current_token) {
            if ($current_token != $token->id) {
                $token->delete();
            }
        });
        return $this->successResponse(200, trans('api.auth.loggedout'), 200, null);
    }

    public function profile()
    {
        return $this->successResponse(200, trans('api.public.done'), 200, new ResourcesCustomer(auth()->user()));
    }


}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\AttRequest;
use App\Models\Attribute;
use App\Models\Option;
use Illuminate\Http\Request;

class AttributeController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $langs = $this->lang;

        $pageConfigs = ['pageHeader' => true];
        return view('backend.attributes.add', compact(['langs', 'pageConfigs','id']));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AttRequest $request)
    {
        try {
            $attribute = new Attribute();
            $attribute->title = $request->title;
            $attribute->product_id = $request->product_id;
            $attribute->save();

            if ($request->options) {
                foreach ($request->options as $item) {
                    $option = new Option();
                    $option->name = $item['name'];
                    $option->price = $item['price'];
                    $option->attribute_id = $attribute->id;
                    $option->save();
                }
            }
            return redirect(route('admin.product.att', $attribute->product_id))->with('success', __('system-messages.add'));
        } catch (\Exception $e) {
            $bug = $e->getMessage();
            return redirect()->back()->with('error', $bug);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show($attribute)
    {
        $langs = $this->lang;
        $attribute = Attribute::findOrFail($attribute);

        return view('backend.attributes.show', compact(['attribute', 'langs']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(AttRequest $request, $attribute)
    {
        try {
            $attribute = Attribute::findOrFail($attribute);
            $attribute->title = $request->title;

            $attribute->save();
            if ($request->option_old) {
                $old_option_ids = [];

                foreach ($request->option_old as $item) {
                    array_push($old_option_ids, $item['id']);
                    $option = Option::find($item['id']);
                    $option->name = $item['name'];
                    $option->price = $item['price'];
                }

                tap(Option::where('attribute_id', $attribute->id)->whereNotIn('id', $old_option_ids)->delete());
            } else {
                tap(Option::where('attribute_id', $attribute->id)->delete());
            }

            if ($request->options) {
                foreach ($request->options as $item) {
                    $option = new Option();
                    $option->name = $item['name'];
                    $option->price = $item['price'];
                    $option->attribute_id = $attribute->id;
                    $option->save();
                }
            }

            return redirect(route('admin.product.att', $attribute->product_id))->with('success', __('system-messages.update'));
        } catch (\Exception $e) {
            $bug = $e->getMessage();
            return redirect()->back()->with('error', $bug);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Attribute $attribute)
    {
        $attribute->delete();
        return redirect(back())->with('success', __('system-messages.delete'));
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Models\Product;
use App\Models\Attribute;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\ProductRequest;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::all();

        $langs = $this->lang;

        $breadcrumbs = [
            ['link' => "admin", 'name' => "Dashboard"], ['name' => "Products"],
        ];

        $addNewBtn = "admin.product.create";

        $pageConfigs = ['pageHeader' => true];

        return view('backend.products.list', compact('products', 'langs', 'pageConfigs', 'breadcrumbs', 'addNewBtn'));

    }

    public function att($id)
    {

        $attributes = Attribute::where('product_id',$id)->get();

        $langs = $this->lang;

        $breadcrumbs = [
            ['link' => "admin", 'name' => "Dashboard"], ['name' => "Attributes"],
        ];

        // $addNewBtn = "admin.attribute.create";

        $pageConfigs = ['pageHeader' => true];

        return view('backend.attributes.list', compact('attributes', 'id','langs', 'pageConfigs', 'breadcrumbs'));

    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $langs = $this->lang;

        $breadcrumbs = [
            ['link' => "admin", 'name' => "Dashboard"], ['link' => "admin/product", 'name' => "Products"], ['name' => "Create"]
        ];

        $pageConfigs = ['pageHeader' => true];
        return view('backend.products.add', compact(['langs', 'pageConfigs', 'breadcrumbs']));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductRequest $request)
    {
        try {
            $product = new Product();
            $product->title = $request->title;
            $product->desc = $request->desc;
            $product->image = $request->hasFile('image') ? uploadFile('product', $request->file('image')) : null;
            $product->save();

            return redirect(route('admin.product.show', $product))->with('success', __('system-messages.add'));
        } catch (\Exception $e) {
            $bug = $e->getMessage();
            return redirect()->back()->with('error', $bug);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        $langs = $this->lang;

        $breadcrumbs = [
            ['link' => "admin", 'name' => "Dashboard"], ['link' => "admin/product", 'name' => "Products"], ['name' => "Update"]
        ];


        return view('backend.products.show', compact(['product', 'langs', 'breadcrumbs']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(ProductRequest $request, Product $product)
    {
        try {
            $product->title = $request->title;
            $product->desc = $request->desc;
            // dd($product->image);
            $product->image = $request->hasFile('image') ? uploadFile('product', $request->file('image'), $product->image) : $product->image;

            $product->save();

            return redirect(route('admin.product.show', $product))->with('success', __('system-messages.update'));
        } catch (\Exception $e) {
            $bug = $e->getMessage();
            return redirect()->back()->with('error', $bug);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $product->delete();
        return redirect(route('admin.product.index'))->with('success', __('system-messages.delete'));
    }
}

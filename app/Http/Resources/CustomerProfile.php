<?php

namespace App\Http\Resources;

use App\Enums\GenderTypes;
use Illuminate\Http\Resources\Json\JsonResource;

class CustomerProfile extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'full_name' => $this->full_name,
        ];
    }
}

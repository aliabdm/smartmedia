<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Product;
use Faker\Generator as Faker;

$factory->define(Product::class, function (Faker $faker) {

    $faker->addProvider(new Xvladqt\Faker\LoremFlickrProvider($faker));

    $filepath = public_path('storage/product');
    if (!File::exists($filepath)) {
        File::makeDirectory($filepath, 0777, true);
    }

    return [
        'image' =>  'product/' . $faker->image($filepath, 600, 600, ['technics'], false),
        'title' => $faker->name,
        'desc' => $faker->realtext()
    ];
});

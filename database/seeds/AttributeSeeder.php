<?php

use App\Models\Attribute;
use App\Models\Option;
use Illuminate\Database\Seeder;

class AttributeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $attributes = Attribute::all();
        foreach ($attributes as $attribute) {
                $option = new Option();
                $option->attribute_id = $attribute->id;
                $option->name = 'large';
                $option->price = rand ( 1 , 100 );                ;
                $option->save();
        }
    }
}

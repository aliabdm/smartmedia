<?php

use App\Models\Product;
use App\Models\Attribute;
use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Product::class, 10)->create();

        $products = Product::all();
        foreach ($products as $product) {
            for ($i=0; $i < 3; $i++) {
                $att = new Attribute();
                $att->product_id = $product->id;
                $att->title = 'option ' . $i;
                $att->save();
            }
        }
    }
}
